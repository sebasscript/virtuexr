export default function traverseScene(children, parentNode) {
  if (children.length === 0) return;

  const childrenNodes = children.map((child, index) => {
    const graphIndex = [...parentNode.graphIndex, index];
    const childNode = {
      name: child.name,
      type: child.type,
      parent: child.parent.name,
      visible: true,
      graphIndex,
    };
    traverseScene(child.children, childNode);
    return childNode;
  });
  parentNode.children = childrenNodes;
  return parentNode;
}
