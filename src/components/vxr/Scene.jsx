import { Suspense } from "react";
import { Stats } from "@react-three/drei";
import Preloader from "./SuspensePreloader";
import Gltfs from "./Gltfs";
import ThreeEntities from "../three/ThreeEntities";
import Controllers from "components/controllers/Controllers";

export default function Scene({ scene, device }) {
  // todo: change performance for low end devices
  // todo:  make this conditional based on framerate minimum or gpu detection
  // const setPixelRatio = useThree((state) => state.setDpr);
  // const current = useThree((state) => state.performance.current);
  // console.log(current);
  // useEffect(() => {
  //   setPixelRatio(window.devicePixelRatio * 0.5);
  // }, [current, setPixelRatio]);

  // todo: whats best way of handling Suspense chains?
  // !make point light threeEntity

  const { gltfs, threeEnt } = {
    gltfs: scene.entities?.gltfs || false,
    threeEnt: scene.entities?.threeEntities || false,
  };

  return (
    <>
      {/* <Suspense fallback={null}> */}
      <Suspense fallback={<Preloader {...scene.preloader} />}>
        {gltfs && <Gltfs gltfs={gltfs} />}
      </Suspense>
      {threeEnt && <ThreeEntities elements={threeEnt} />}
      <Controllers scene={scene} device={device} />
      {scene?.tools?.stats && <Stats />}
      {/* <Postprocessing /> */}
    </>
  );
}
