import { useEffect } from "react";
import { useMapStore } from "store/useMapStore";
import { useLocation } from "react-router";

// get all scene descriptions
import * as Views from "views";

export default function useSetActiveView() {
  // todo: at fetch from public folder for to test and setup async fetch
  const { setActiveView, activeView } = useMapStore([
    "setActiveView",
    "activeView",
  ]);

  const { pathname } = useLocation();
  const routeKey = pathname.split("/")[1];

  useEffect(() => {
    setActiveView(Views[routeKey]);
    return () => {
      setActiveView(null);
    };
  }, [routeKey, setActiveView]);

  return activeView;
}
