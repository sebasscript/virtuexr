import { useRef, useEffect } from "react";
import { useGLTF, useAnimations } from "@react-three/drei";
import { useMapStore } from "store/useMapStore";
import { LoopOnce } from "three";
import useAddMixerEvents from "./useAddMixerEvents";
import SceneObjects from "threejs/sceneObjects";
import useEnableTransformControls from "components/three/useEnableTransformControls";

export default function Gltfs({ gltfs }) {
  const models = Object.entries(gltfs).map(([key, model], index) => {
    return <Model name={key} model={model.config} key={index} />;
  });

  return <>{models}</>;
}

function Model({ model, name }) {
  const primitive = useRef();
  const { addTransformControls, removeTransformControls } =
    useEnableTransformControls(primitive, name);

  const position = model?.position || [0, 0, 0];
  const scale = model?.scale || 1;
  const rotation = model?.rotation || [0, 0, 0];

  const { scene, animations } = useGLTF(model.path, "/draco/");
  useAddToSceneObjects(name, scene);
  const { actions, mixer } = useAnimations(animations, primitive);
  useAddMixerEvents(mixer, name);

  // todo: move this into animations component
  // play animations after load
  useEffect(() => {
    // const actionsList = Object.keys(actions); //memorize this
    // actionsList.forEach((action) => actions[action]?.play());
    model.playAnimations?.forEach((action) => actions[action]?.play());
  }, [actions, model]);

  function onClick() {
    if (model.enableTransforms) addTransformControls();
  }
  function onPointerMissed() {
    if (model.enableTransforms) removeTransformControls();
  }

  return (
    <>
      <primitive
        ref={primitive}
        object={scene}
        position={position}
        scale={scale}
        rotation={rotation}
        onClick={onClick}
        onPointerMissed={onPointerMissed}
      />
      <Animations actions={actions} mixer={mixer} modelName={name} />
    </>
  );
}

function Animations({ actions, modelName, mixer }) {
  // todo: use zustand selector useStore(state => activeModelAnim[modelName])
  // to avoid unnecessary re-renders
  const { activeModelAnim, dispatch } = useMapStore([
    "activeModelAnim",
    "dispatch",
  ]);

  useEffect(() => {
    if (activeModelAnim[modelName]) {
      Object.entries(activeModelAnim[modelName]).forEach(
        ([animation, value]) => {
          // todo: add error catch for missing animations
          const thisAction = actions[animation];

          if (!value.active || value?.playing) return null;
          if (!value.loop) {
            thisAction.reset();
            thisAction.setLoop(LoopOnce);
            dispatch({
              actions: [
                {
                  dispatch: "setActiveModelAnim",
                  config: {
                    modelName,
                    animation,
                  },
                  payload: {
                    active: false,
                    playing: true,
                  },
                },
              ],
            });
          }
          if (value?.effectiveTimeScale)
            thisAction.setEffectiveTimeScale(value.effectiveTimeScale);
          if (value?.startAt) {
            thisAction.startAt(value.startAt + mixer.time);
          }
          thisAction.play();
        }
      );
    }
  }, [activeModelAnim, modelName, actions, mixer.time, dispatch]);
  return null;
}

function useAddToSceneObjects(modelName, scene) {
  const added = useRef(false);
  useEffect(() => {
    if (!scene || added.current) return;
    SceneObjects.addScene(modelName, scene);
    return () => SceneObjects.removeScene(modelName);
  }, [scene, modelName]);
}
