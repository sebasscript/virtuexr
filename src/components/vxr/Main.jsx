import { useRef, useEffect } from "react";
import { Canvas } from "@react-three/fiber";
import Scene from "./Scene";

import Ui from "components/ui";
import Inputs from "components/inputs";
import Preloader from "./Preloader";
import Audio from "components/controllers/Audio";
import useDevice from "components/controllers/hooks/useDevice";
import { resetStore } from "store/useStore";
import useSetActiveView from "./hooks/useSetActiveView";
// import UploadModal from "components/ui/uploads/UploadMoal";

export default function Main() {
  console.log("rerender main component");
  const ref = useRef();

  const device = useDevice();
  const activeView = useSetActiveView();

  // todo: turn into hook
  // todo: improve reset function
  // reset app store
  useEffect(() => {
    return () => {
      resetStore();
    };
  }, []);

  // todo: turn into hook
  // set overflows
  useEffect(() => {
    document.documentElement.style.overflowY = "hidden";
    document.body.overflowY = "hidden";
    document.querySelector("#root").style.overflowY = "hidden";
  }, []);

  if (!activeView) return null;
  const { scene, inputs, ui, audio } = activeView;
  const { position, near, far, fov } = scene.camera.config;
  const { frameloop } = scene.render.config;

  return (
    <div className="xl:h-screen xl:w-screen w-full h-full z-10 overflow-hidden bg-black">
      <Canvas
        ref={ref}
        camera={{
          position: position[device.size],
          near,
          far,
          fov,
        }}
        frameloop={frameloop}
      >
        <Scene scene={scene} device={device} />
      </Canvas>
      <Preloader scene={scene} />
      <Ui ui={ui} device={device} />
      <Inputs inputs={inputs} device={device} />
      <Audio audio={audio} />
      {/* <UploadModal /> */}
    </div>
  );
}
