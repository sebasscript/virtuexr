import { useEffect, useRef } from "react";
import { useMapStore } from "store/useMapStore";

export default function useAddMixerEvents(mixer, modelName) {
  const firstRender = useRef(true);
  const { dispatch } = useMapStore(["dispatch"]);

  useEffect(() => {
    function updateAnimStateOnFinish(e) {
      dispatch({
        actions: [
          {
            dispatch: "setActiveModelAnim",
            config: {
              modelName,
              animation: e.action._clip.name,
            },
            payload: {
              active: false,
              playing: false,
            },
          },
        ],
      });
    }

    if (firstRender.current && mixer) {
      mixer.addEventListener("finished", updateAnimStateOnFinish);
      firstRender.current = false;
    }
    return () => {
      mixer.removeEventListener("finished", updateAnimStateOnFinish);
    };
  }, [mixer, dispatch, modelName]);
}
