import { useMapStore } from "store/useMapStore";

export default function Button({ events, config, parent, elKey }) {
  const { dispatch } = useMapStore(["dispatch"]);
  // const bg = props.playing ? "bg-green-600" : "bg-white bg-opacity-30";

  // todo: is there an easier/less hand typed way to construct the state path?
  const eventPath = `ui.${parent}.entities.${elKey}.events`;

  const onClick = (e) => {
    const actions = events?.onClick?.actions;
    if (!actions) return;
    const path = `${eventPath}.onClick.actions`;
    const name = events.onClick?.config?.name || "onClick";
    dispatch({ actions, path, name });
    e.target.blur();
  };

  const onTouchStart = (e) => {
    const actions = events?.onTouchStart?.actions;
    if (!actions) return;
    const path = `${eventPath}.onTouchStart.actions`;
    const name = events.onTouchStart?.config?.name || "onClick";
    dispatch({ actions, path, name });
    e.target.blur();
  };

  const onTouchEnd = (e) => {
    const actions = events?.onTouchEnd?.actions;
    const path = `${eventPath}.onTouchEnd.actions`;
    const name = events.onTouchEnd?.config?.name || "onClick";
    dispatch({ actions, path, name });
    e.target.blur();
  };

  let format;
  let visibility;
  if (config.large)
    format =
      "text-lg md:text-3xl 2xl:text-4xl px-7 py-2 pt-1 bg-white bg-opacity-30 backdrop-filter backdrop-blur-xl border-2 border-white border-opacity-70 shadow-2xl hover:bg-white hover:bg-opacity-30 hover:border-black hover:border-opacity-80 hover:shadow-lg";
  else if (config.round)
    format =
      "text-xs md:text-base md:text-md p-4 rounded-full bg-white bg-opacity-30 background focus:outline-none select-none";
  else
    format =
      "text-xs md:text-base md:text-md py-1 px-4 bg-white bg-opacity-30 background ";

  if (config.mobileOnly) visibility = "inline xl:hidden";

  // todo: conditionally add null or function to callback base on existing props
  // onClick={props.events?.onTouchStart ? onTouchStart : null}
  return (
    <button
      onClick={events?.onClick ? onClick : null}
      onTouchStart={events?.onTouchStart ? onTouchStart : null}
      onTouchEnd={events?.onTouchEnd ? onTouchEnd : null}
      // onMouseDown={onMouseDown ? onMouseDown : null}
      // onMouseUp={onMouseUp ? onMouseUp : null}
      className={`${format} ${visibility} text-left rounded-3xl block mb-3 w-full hover:bg-opacity-60 text-white select-none`}
    >
      {config.name}
      {config?.icon && (
        <img src={config.icon} className={`w-8 rounded-full`} alt="icon" />
      )}
    </button>
  );
}
