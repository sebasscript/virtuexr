import Button from "./Button";
import Modal from "./Modal";

export default function Ui({ ui, device }) {
  // todo: wrap into use callback??
  const menuComponents = () => {
    const menu = ui?.menu;
    // iterate through all ui groups
    const allUiElements = Object.keys(menu).map((group) => {
      const uiGroup = menu[group];

      const entities = uiGroup.entities;
      // all children of group
      const children = Object.entries(entities).map(([key, props], index) => {
        // todo:  add component selection based on element type
        //  if (props.type === "button") then.....

        if (!props?.active) return null;
        if (props?.config?.touchOnly && !device.isTouch) return null;
        return (
          <Button key={key} elKey={key} index={index} parent={group} {...props}>
            {key}
          </Button>
        );
      });

      // position of group
      let pos;
      switch (uiGroup.config.position) {
        case "topLeft":
          pos = "top-0 left-0 xl:top-3 xl:left-3";
          break;
        case "bottomCenter":
          pos = "bottom-1 flex justify-center w-full";
          break;
        default:
          pos = "top-3 left-3";
      }

      // styling of group
      let style;
      if (uiGroup.config.frame)
        style = `border-2 p-2  rounded-br-xl rounded-tr-xl xl:rounded-tl-xl xl:rounded-bl-xl  bg-white bg-opacity-10 backdrop-filter backdrop-blur-xl`;

      //all groups with children
      // todo: just check if name exists, if not then don't return name prop
      // removes extra GroupName props need
      return (
        <div className={`absolute ${pos}`} key={group}>
          <div className={`${style}`}>
            <div className="text-white text-lg mb-2 text-center select-none ">
              {uiGroup?.config?.groupName && uiGroup?.config?.name}
            </div>
            <div>{children}</div>
          </div>
        </div>
      );
    });

    return allUiElements;
  };

  return (
    <>
      <Modal />
      {menuComponents()}
    </>
  );
}
