import { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import createFileMap from "utils/createFileMap";
// import { useMapState } from "hooks/useMapState";

export default function UploadGltfButton({ closeModal }) {
  // const state = useMapState("setFileMaps");

  const onDrop = useCallback(
    (acceptedFiles) => {
      closeModal();
      const { name, rootFile, map } = createFileMap(acceptedFiles);
      // todo: add gltf into view graph
      // state.setFileMaps(name, rootFile, map);
    },
    [closeModal]
  );

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <div className="text-center">
      <button
        {...getRootProps()}
        className="text-center text-md inline-flex justify-center focus:outline-none bg-gradient-to-r from-purple-700 to-blue-700 px-5 py-1 rounded-2xl text-white"
        autoFocus
      >
        Upload GLTF Folder
        {/* </button> */}
        <input
          {...getInputProps()}
          className="block"
          directory=""
          webkitdirectory=""
        />
      </button>
    </div>
  );
}
