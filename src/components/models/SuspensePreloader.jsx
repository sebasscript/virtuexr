import { useEffect } from "react";
import { useProgress } from "@react-three/drei";
import { useMapStore } from "store/useMapStore";

export default function Preloader({
  backgroundImg,
  img,
  text,
  disabled = false,
}) {
  const store = useMapStore(["setPreloader"]);
  const loader = useProgress();
  const { active, progress } = loader;

  useEffect(() => {
    store.setPreloader({
      active,
      progress,
      backgroundImg,
      img,
      text,
    });
    return () => {
      store.setPreloader({
        active: false,
      });
    };
  }, [store, progress, backgroundImg, img, active, loader, text]);

  if (disabled) return null;
  return null;
}
