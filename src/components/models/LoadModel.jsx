import { useEffect, useRef, useState } from "react";
import { useLoader } from "@react-three/fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import traverseScene from "utils/traverseScene";
import { useMapState } from "hooks/useMapState";

export default function LoadModel({ fileMap }) {
  // todo: can remove this once re renders are fixed
  // todo: and indexes can be received from parent
  const initialLoad = useRef(true);
  const [index, setIndex] = useState(null);

  const { modelGraphs, addModelGraph, updateNodeAttributes } = useMapState(
    "modelGraphs",
    "addModelGraph",
    "updateNodeAttributes"
  );

  const objectURLs = [];
  const { scene, asset } = useLoader(GLTFLoader, fileMap.rootFile, (loader) => {
    loader.manager.setURLModifier((url) => {
      // url is relative path to rooFile
      url = URL.createObjectURL(fileMap.map[url]);
      objectURLs.push(url);
      return url;
    });
    objectURLs.forEach((url) => URL.revokeObjectURL(url));
  });

  useEffect(() => {
    if (scene && initialLoad.current) {
      const rootNode = {
        name: scene.name,
        type: scene.type,
        parent: scene.parent.name,
        visible: true,
        graphIndex: [0],
      };
      scene.visible = true;
      if (scene.children.length === 0) return;
      const sceneNodes = traverseScene(scene.children, rootNode);
      //todo: get name from fileMap.name
      // console.log("in component");
      // console.log(fileMap.rootFile);
      const name = asset.extras?.title || fileMap.name;

      // todo: need to factor this out as causes re renders
      const index = modelGraphs.length + 1;
      setIndex(index);

      addModelGraph(index, name, true, sceneNodes);
      initialLoad.current = false;
    }
  }, [scene, asset.extras, addModelGraph, modelGraphs, fileMap.name]);

  useEffect(() => {
    updateNodeAttributes.forEach((update) => {
      if (update?.modelIndex === index) {
        const rootNode = scene;
        let node;
        update.graphIndex.forEach((el, index) => {
          if (index === 0) {
            node = rootNode;
          } else {
            node = node.children[el];
          }
        });
        node.visible = update.visible;
      }
    });
  }, [updateNodeAttributes, index, scene]);

  // todo: figure out why need to set visible to false to avoid change of visibility
  // on new model upload
  return <primitive object={scene} visible={false} />;
}
