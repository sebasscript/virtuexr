import { Sky } from "@react-three/drei";

export default function SkyShader() {
  console.log("render sky");
  return (
    <Sky
      distance={450000}
      sunPosition={[0, 0, 10]}
      inclination={0.2}
      azimuth={0.75}
      turbidity={0.2}
    />
  );
}
