import { GridHelper } from "three";
import { extend } from "@react-three/fiber";

extend({ GridHelper });

export default function Grid({ config, name }) {
  return <gridHelper args={[config.size, config.divisions]} />;
}
