import { Suspense } from "react";
import { Environment } from "@react-three/drei";

export default function Test({ config, name }) {
  //   if (!scene.entities.threeEntities?.environment) return null;
  return (
    <Suspense fallback={null} key={name}>
      <Environment path={config.path} files={config.files} key={name} />
    </Suspense>
  );
}
