import SkyDome from "./SkyDome";
import Plane from "./Plane";
import Environment from "./Environment";
import Grid from "./Grid";
import SkyShader from "./SkyShader";

export default function ThreeEntities({ elements }) {
  const el = Object.entries(elements).map(([key, value]) =>
    mapType(value.type, value.config, key)
  );

  return <>{el}</>;
}

function mapType(type, config, key) {
  const attr = {
    config,
    name: key,
    key: key,
  };

  switch (type) {
    case "plane":
      return <Plane {...attr} />;
    case "skyDome":
      return <SkyDome {...attr} />;
    case "pointLight":
      return <pointLight {...attr} />;
    case "ambientLight":
      return <ambientLight {...attr} />;
    case "environment":
      return <Environment {...attr} />;
    case "grid":
      return <Grid {...attr} />;
    case "skyShader":
      return <SkyShader />;
    default:
      return null;
  }
}
