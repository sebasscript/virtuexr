import useSound from "use-sound";
import { useEffect, useState } from "react";
import { useMapStore } from "store/useMapStore";

export default function Audio({ audio }) {
  const audioComponents = Object.entries(audio).map(
    ([name, { config, state }]) => (
      <AudioComponent name={name} config={config} state={state} key={name} />
    )
  );
  return <>{audioComponents}</>;
}

function AudioComponent({ name, config, state }) {
  const { activeAudio, dispatch } = useMapStore(["activeAudio", "dispatch"]);
  const [playing, setPlaying] = useState(false);

  const [play, { stop }] = useSound(config.url, {
    ...config,
    onend: onEnd,
  });

  function onEnd() {
    if (config.playTillEnd) {
      setPlaying(false);
    }
  }

  useEffect(() => {
    if (name in activeAudio && !playing) {
      play();
      if (config?.loop) setPlaying(true);
      // if no loop remove audio items from state obj
      else if (config.playTillEnd) {
        setPlaying(true);
        dispatch({
          actions: [
            {
              dispatch: "setActiveAudio",
              payload: {
                name,
              },
            },
          ],
        });
      } else
        dispatch({
          actions: [
            {
              dispatch: "setActiveAudio",
              payload: {
                name,
              },
            },
          ],
        }); //remove if no loop
    }
    if (!(name in activeAudio) && playing && !config.playTillEnd) {
      stop();
      if (config.loop) setPlaying(false);
    }
    //if no loop remove from state here
    //stop if loop and update removed track from audio
    //track if track is playing with local storage for now
  }, [
    play,
    activeAudio,
    name,
    playing,
    stop,
    config.loop,
    config.useOnce,
    config.playTillEnd,
    dispatch,
  ]);

  return null;
}
