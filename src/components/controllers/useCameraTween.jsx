import { useThree } from "@react-three/fiber";
import { useSpring } from "@react-spring/three";
import { useMapStore } from "store/useMapStore";
import { useEffect } from "react";

export default function useCameraTween(size) {
  const { cameraTween, dispatch } = useMapStore(["cameraTween", "dispatch"]);
  const { state } = cameraTween || {};

  const { camera } = useThree();
  const { x, y, z } = camera.position;

  // todo: abstract this into tween hook that can be used elsewhere
  // eslint-disable-next-line
  const [styles, tween] = useSpring(() => ({
    loop: false,
    position: [0, 0, 0],
    onChange: ({ value }) => {
      const pos = value.position;
      camera.position.set(pos[0], pos[1], pos[2]);
    },
  }));

  // todo:  only tween if values are different from previous tween
  useEffect(() => {
    if (!state?.active) return;
    tween.start({
      config: { duration: state.duration },
      from: {
        position: [x, y, z],
      },
      to: {
        position: state.position[size],
      },
      onRest: () => {
        tween.stop();
        if (cameraTween?.onComplete) {
          const onCompletePath = `${cameraTween.path}.onComplete`;
          dispatch({
            actions: cameraTween.onComplete,
            path: onCompletePath,
            name: "tween onComplete",
          });
        }
        const action = {
          target: "cameraTween",
          payload: { active: false, position: [0, 0, 0] },
        };
        dispatch({
          actions: [action],
          path: cameraTween.path,
          name: "reset cameraTween",
        });
      },
    });
  }, [tween, x, y, z, cameraTween, dispatch, state, size]);
}
