import { useMediaQuery } from "react-responsive";
import { useMapStore } from "store/useMapStore";

export default function useResponsive() {
  const { dispatch } = useMapStore(["dispatch"]);
  // const isTablet = useMediaQuery({
  //   query: "(min-width: 768px) and (max-width: 1024px)",
  // });
  const isDefault = useMediaQuery({
    query: "(min-width: 768px)",
  });

  const isTouch = useMediaQuery({
    query: "(pointer: coarse), (hover:none)",
  });

  const size = isDefault ? "default" : "mobile";

  dispatch({
    actions: [
      {
        target: "device",
        payload: {
          size,
          isTouch,
        },
      },
    ],
  });

  return {
    size,
    isTouch,
  };
}
