import { useEffect } from "react";
import { useMapStore } from "store/useMapStore";

export default function useAfterPreloader(isActive, afterPreloader) {
  const { dispatch } = useMapStore(["dispatch"]);

  useEffect(() => {
    if (!isActive || afterPreloader?.actions) {
      dispatch({
        actions: afterPreloader.actions,
        name: "afterPreloader",
      });
    }
    // eslint-disable-next-line
  }, []);
}
