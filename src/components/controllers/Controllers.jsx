import TransformController from "components/controllers/TransformController";
import RenderController from "../controllers/RenderController";
import useCameraTween from "./useCameraTween";
import OrbitCamController from "./OrbitCamController";
import FpsController from "./FpsController";
import Postprocessing from "components/controllers/Postprocessing";

export default function Controllers({ scene, device }) {
  const {
    transformController,
    fpsController,
    orbitController,
    postprocessingController,
  } = scene.controllers;

  useCameraTween(device.size);

  return (
    <>
      <RenderController render={scene.render} />
      {postprocessingController?.active && (
        <Postprocessing config={postprocessingController.config} />
      )}
      {orbitController?.active && (
        <OrbitController scene={scene} device={device} />
      )}
      {transformController?.active && (
        <TransformController config={transformController.config} />
      )}
      {fpsController?.active && <FpsController />}
    </>
  );
}

function OrbitController({ scene, device }) {
  return (
    <OrbitCamController
      camera={scene.camera}
      controllers={scene.controllers}
      device={device}
      makeDefault={scene.controllers.orbitController.config.makeDefault}
    />
  );
}
