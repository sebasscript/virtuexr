import { useSpring } from "@react-spring/three";
import { useMapStore } from "store/useMapStore";
import { Vector3 } from "three";
import { useEffect } from "react";

export default function useTargeOrbitTween(orbitRef, initTarget, size) {
  const { cameraTargetTween, camera, dispatch } = useMapStore([
    "cameraTargetTween",
    "camera",
    "dispatch",
  ]);
  const { target, duration, active } = cameraTargetTween?.state || {};
  const currentTarget = camera?.state?.target || initTarget; //target is the default scene cam target

  // todo: extract this into separate hook with refactored generall hook for tween
  //eslint-disable-next-line
  const [targetStyles, targetTween] = useSpring(() => ({
    loop: false,
    target: currentTarget,
    onChange: ({ value }) => {
      const target = value.target;
      orbitRef.current.target = new Vector3(...target);
    },
    onRest: ({ value }) => {
      targetTween.stop();
      const updatedTween = { ...cameraTargetTween };
      updatedTween.active = false;
      dispatch({
        actions: [
          {
            target: "cameraTargetTween",
            payload: updatedTween,
          },
          {
            target: "camera",
            payload: { target: value.target },
          },
        ],
        name: "tweenTarget complete",
      });
    },
  }));

  useEffect(() => {
    if (!active) return;
    targetTween.start({
      config: { duration },
      from: {
        target: currentTarget,
      },
      to: {
        target: target[size],
      },
    });
  }, [active, target, size, targetTween, currentTarget, duration]);
}
