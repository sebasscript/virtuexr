import SceneObjects from "threejs/sceneObjects";
import { TransformControls } from "@react-three/drei";
import { useMapStore } from "store/useMapStore";
import { useRef, useLayoutEffect } from "react";

export default function TransformController({ config }) {
  const controlsRef = useRef();
  const { transFormController } = useMapStore(["transFormController"]);

  useLayoutEffect(() => {
    if (!transFormController.active) {
      controlsRef.current.detach();
      return;
    }
    if (config?.logUpdates) console.log(transFormController.name);
    const attachObj = SceneObjects.transformControlsObj;
    controlsRef.current?.attach(attachObj);
    controlsRef.current.setMode(transFormController.mode);
  }, [transFormController, config]);

  function logUpdates(e) {
    if (!config?.logUpdates) return;
    console.log(controlsRef.current.object.position);
    console.log(controlsRef.current.object.rotation);
  }

  return <TransformControls ref={controlsRef} onObjectChange={logUpdates} />;
}
