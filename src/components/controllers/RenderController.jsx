import { useMemo } from "react";
import { useThree } from "@react-three/fiber";
import { useControls } from "leva";
// todo: does this ramp up the bundle size?
import * as THREE from "three";

export default function RenderController({
  controls = false,
  render,
  ...props
}) {
  const { config } = render;
  if (controls) return <WithControls {...config} {...props} />;
  return <WithProps {...config} {...props} />;
}
function WithProps({ toneMappingExposure, toneMapping }) {
  const { gl } = useThree();
  useMemo(() => {
    toneMappingExposure && (gl.toneMappingExposure = toneMappingExposure);
    toneMapping && (gl.toneMapping = THREE[toneMapping]);
  }, [gl, toneMappingExposure, toneMapping]);
  return null;
}

function WithControls({ toneMapExp, ...props }) {
  const { exposure } = useControls({
    exposure: {
      value: toneMapExp,
      min: 0,
      max: 3,
      step: 0.01,
    },
  });
  const { gl } = useThree();
  useMemo(() => (gl.toneMappingExposure = exposure), [gl, exposure]);
  return null;
}
