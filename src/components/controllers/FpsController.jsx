import { PointerLockControls } from "three-stdlib";
import { extend, useThree, useFrame } from "@react-three/fiber";
import { useRef, useEffect, useMemo } from "react";
import { useMapStore } from "store/useMapStore";
import { Vector3, Raycaster, Euler } from "three";
import SceneObjects from "threejs/sceneObjects";
// import {useSubscribe} from 'store/useMapStore'

extend({ PointerLockControls });

const direction = new Vector3();
const velocity = new Vector3();

const xDir = new Vector3();
const yDir = new Vector3(0, 1, 0);
const negYDir = new Vector3(0, -1, 0);
const zDir = new Vector3();

const avatarHeight = 2.2;

const rayX = new Raycaster(new Vector3(), new Vector3(), 0, 4.8);
const rayY = new Raycaster(new Vector3(), new Vector3(), 0, 1.2);
const rayNegY = new Raycaster(new Vector3(), new Vector3(), 0, avatarHeight);
const rayZ = new Raycaster(new Vector3(), new Vector3(), 0, 4);
const rayZAngle = new Raycaster(new Vector3(), new Vector3(), 0, 5);

rayNegY.firstHitOnly = true;
rayX.firstHitOnly = true;
rayY.firstHitOnly = true;
rayZ.firstHitOnly = true;
rayZAngle.firstHitOnly = true;

export default function FpsController(props) {
  const controlsRef = useRef();
  const canJump = useRef(true);
  // const wait = useTimer(500);

  const {
    fpsController: {
      forward,
      backward,
      left,
      right,
      jump,
      rotateUp,
      rotateDown,
      rotateLeft,
      rotateRight,
    },
    device,
  } = useMapStore(["fpsController", "device"]);

  // const {
  //   forward,
  //   backward,
  //   left,
  //   right,
  //   jump,
  //   rotateUp,
  //   rotateDown,
  //   rotateLeft,
  //   rotateRight,
  // } = useSubscribe("fpsController");

  // todo: get the scene name from config
  // todo: wrap this so it only runs once, and only again if new objects are added
  const sceneObjects = SceneObjects.getScenes().gallery;

  // todo: get intersection object names from view.config
  const intersectionObjects = useMemo(() => {
    return find3DObjects(sceneObjects, ["glass", "bridge_body001"]);
  }, [sceneObjects]);

  //handle pointer lock
  const {
    camera,
    gl: { domElement },
  } = useThree();

  useEffect(() => {
    if (!domElement) return;
    if (device.isTouch) return;
    controlsRef.current.connect();
    function activatePointerLock() {
      controlsRef.current.lock();
    }
    domElement.addEventListener("click", activatePointerLock);
    return () => domElement.removeEventListener("click", activatePointerLock);
  }, [domElement, device]);

  // handle jumping
  useEffect(() => {
    if (jump && canJump.current) {
      velocity.y += 50;
      canJump.current = false;
    }
  }, [jump, canJump]);

  // update motion on each frame
  // todo: run all the speed dampers conditionally? if speed small enought, don't run and set speed to 0
  useFrame((state, delta) => {
    if (delta > 0.04) delta = delta / 1.8;
    if (delta > 0.1) delta = delta / 3.5;

    const controls = controlsRef.current;
    const controlsObject = controlsRef.current.getObject(); //equivalent to camera

    //x-z direction vector
    direction.x = Number(right) - Number(left);
    direction.z = Number(forward) - Number(backward);
    direction.normalize();

    // damping + gravity
    velocity.x -= velocity.x * 10.0 * delta; //damping
    velocity.z -= velocity.z * 10.0 * delta; //damping
    velocity.y -= 9.8 * 16.0 * delta; // gravity: -g * dT * massFactor;

    // get controls current z direction
    // if (left || right || forward || backward) {
    controlsObject.getWorldDirection(zDir);
    zDir.y = 0; //only want the direction in the xz plane
    zDir.normalize();
    // }

    //x motion
    if (left || right) {
      xDir.copy(zDir);
      if (left) xDir.applyEuler(new Euler(0, Math.PI / 2, 0));
      if (right) xDir.applyEuler(new Euler(0, -Math.PI / 2, 0));
      rayX.set(controlsObject.position, xDir);
      const intersectX = rayX.intersectObjects(intersectionObjects, false);
      if (!(intersectX.length > 0)) velocity.x -= direction.x * delta * 170;
    }

    //z motion
    if (forward || backward) {
      if (backward) zDir.negate();
      rayZ.set(controlsObject.position, zDir);
      const intersectZ = rayZ.intersectObjects(intersectionObjects, false);

      // 45 degree left
      rayZAngle.set(
        controlsObject.position,
        zDir.applyEuler(new Euler(0, Math.PI / 4, 0))
      );
      const intersectZLeft = rayZ.intersectObjects(intersectionObjects, false);

      // 45 degree right
      rayZAngle.set(
        controlsObject.position,
        zDir.applyEuler(new Euler(0, -Math.PI / 4, 0))
      );
      const intersectZRight = rayZ.intersectObjects(intersectionObjects, false);

      const intersectAll = intersectZ
        .concat(intersectZLeft)
        .concat(intersectZRight);

      if (!(intersectAll.length > 0)) velocity.z -= direction.z * delta * 170;
    }

    //y motion
    // floor
    rayNegY.set(controlsObject.position, negYDir);
    const intersectFloor = rayNegY.intersectObjects(intersectionObjects, false);
    if (intersectFloor.length > 0) {
      velocity.y = Math.max(0, velocity.y);
      if (velocity.y === 0) canJump.current = true; //do not reset on upward motion

      const targetHeight = avatarHeight - intersectFloor[0]?.distance;
      if (targetHeight > 0.1) velocity.y += targetHeight * 4;
    }
    // ceiling
    if (velocity.y > 20) {
      rayY.set(controlsObject.position, yDir);
      const intersectCeiling = rayY.intersectObjects(
        intersectionObjects,
        false
      );
      const touchCeiling = intersectCeiling.length > 0;
      if (touchCeiling) {
        velocity.y = -9.8 * 16.0 * delta;
      }
    }

    // move camera
    controls.moveRight(-velocity.x * delta);
    controls.moveForward(-velocity.z * delta);
    controlsObject.position.y += velocity.y * delta;

    // rotate camera for non pointerlock controls
    if (!(rotateUp || rotateDown || rotateLeft || rotateRight)) return;
    const _euler = new Euler(0, 0, 0, "YXZ");
    _euler.setFromQuaternion(controlsObject.quaternion);
    if (rotateUp) _euler.x += delta * 0.3;
    if (rotateDown) _euler.x -= delta * 0.3;
    if (rotateLeft) _euler.y += delta * 0.8;
    if (rotateRight) _euler.y -= delta * 0.8;

    // limit angle
    _euler.x = Math.max(-Math.PI / 2, Math.min(Math.PI / 2, _euler.x));
    camera.quaternion.setFromEuler(_euler);
  });

  return (
    <pointerLockControls
      ref={controlsRef}
      args={[camera, domElement]}
      makeDefault
    />
  );
}

function find3DObjects(scene, names) {
  const objects = [];
  names.forEach((name) => {
    const index = scene.children.findIndex((child) => child.name === name);
    const match = scene.children[index];
    if (match.children.length === 0) {
      scene.children[index].geometry.computeBoundsTree();
      objects.push(scene.children[index]);
    } else {
      match.children.forEach((child) => {
        //todo: iterate to bottom of the tree, if child.children.length > 0, then recurse
        if (child.type === "Mesh") {
          child.geometry.computeBoundsTree();
          objects.push(child);
        }
      });
    }
  });
  return objects;
}

// function useTimer(time) {
//   const [wait, setWait] = useState(true);
//   const firstExec = useRef(true);

//   useEffect(() => {
//     if (!firstExec.current) return;
//     firstExec.current = false;
//     setTimeout(() => {
//       setWait(false);
//     }, time);
//   });

//   return wait;
// }
