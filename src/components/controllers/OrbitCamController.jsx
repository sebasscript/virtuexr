import { OrbitControls } from "@react-three/drei";
import { useThree } from "@react-three/fiber";
import { useEffect, useRef } from "react";
import { useControls } from "leva";
import useTargetOrbitTween from "./useTargetOrbitTween";

export default function OrbitCamController({
  camera,
  controllers,
  device,
  makeDefault,
}) {
  const orbitControls = controllers.orbitController.config;
  //todo: can the WithControls be moved into separate, independent component?
  if (camera.uiControls)
    return (
      <WithControls
        camera={camera}
        orbitControls={orbitControls}
        size={device.size}
        makeDefault={makeDefault}
      />
    );
  return (
    <WithProps
      camera={camera}
      orbitControls={orbitControls}
      size={device.size}
      makeDefault={makeDefault}
    />
  );
}

function WithProps({ orbitControls, size, makeDefault }) {
  const orbitRef = useRef();
  const {
    enableZoom,
    enablePan,
    maxAzimuthAngle,
    minAzimuthAngle,
    maxPolarAngle,
    minPolarAngle,
    target,
  } = orbitControls;

  useTargetOrbitTween(orbitRef, target[size], size);

  return (
    <>
      <OrbitControls
        ref={orbitRef}
        enableZoom={enableZoom}
        enablePan={enablePan}
        maxAzimuthAngle={maxAzimuthAngle}
        minAzimuthAngle={minAzimuthAngle}
        maxPolarAngle={maxPolarAngle}
        minPolarAngle={minPolarAngle}
        target={target[size]}
        screenSpacePanning={false}
        makeDefault={makeDefault}
      />
    </>
  );
}

function WithControls({
  camera: cam,
  orbitControls,
  size,
  makeDefault,
  ...props
}) {
  const { fov, near, far, position } = cam;
  const { target } = orbitControls;
  const { camPosition, targetControl, camFrustum } = useControls({
    camPosition: {
      x: position[size][0],
      y: position[size][1],
      z: position[size][2],
    },
    targetControl: {
      x: target[size][0],
      y: target[size][1],
      z: target[size][2],
    },
    camFrustum: { fov, near, far },
  });

  const { camera, gl } = useThree();
  useEffect(() => {
    camera.fov = camFrustum.fov;
    camera.near = camFrustum.near;
    camera.far = camFrustum.far;
    camera.position.x = camPosition.x;
    camera.position.y = camPosition.y;
    camera.position.z = camPosition.z;
    camera.updateProjectionMatrix();
  }, [camFrustum, camera, gl, camPosition]);

  return (
    <OrbitControls
      target={Object.values(targetControl)}
      makeDefault={makeDefault}
    />
  );
}
