import { useEffect } from "react";
import { useMapStore } from "store/useMapStore";

export default function useViewStoreFifo() {
  const {
    viewStoreFifo,
    shiftViewStoreFifo,
    updateViewStore,
    updateViewsStoreTarget,
  } = useMapStore([
    "viewStoreFifo",
    "shiftViewStoreFifo",
    "updateViewsStoreTarget",
    "updateViewStore",
  ]);

  useEffect(() => {
    if (viewStoreFifo.length === 0) return;
    const nextEl = viewStoreFifo[0];
    if (nextEl.actionPath) {
      updateViewStore(
        nextEl.action,
        nextEl.actionPath,
        nextEl.index,
        nextEl.update
      );
    } else updateViewsStoreTarget(nextEl.action);
    shiftViewStoreFifo();
  }, [
    viewStoreFifo,
    shiftViewStoreFifo,
    updateViewStore,
    updateViewsStoreTarget,
  ]);
}

// state setup
// viewStoreFifo: [],
// addToViewStoreFifo: (action, actionPath, index, update) =>
//   set(
//     produce((state) => {
//       state.viewStoreFifo.push({ action, actionPath, index, update });
//     })
//   ),
// shiftViewStoreFifo: () =>
//   set(
//     produce((state) => {
//       state.viewStoreFifo.shift();
//     })
//   ),
