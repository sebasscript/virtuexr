export function NFTList({ nftList }) {
  const header = <header key="header">NFT Dashboard</header>;
  const main = (
    <main key="main">
      <div className="mt-6 md:mt-10">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5">
          {nftList}
        </div>
      </div>
    </main>
  );
  const button = <div key="buton">Close</div>;

  const children = [header, main, button];
  return children;
}
