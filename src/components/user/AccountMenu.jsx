import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { getEllipsisTxt } from "helpers/formatters";
import { useMoralis } from "react-moralis";
import { ChevronDownIcon } from "@heroicons/react/solid";
import MenuItem from "./MenuItem";
import { useLocation } from "react-router-dom";

export default function AccountMenu({ setIsOpen, setModalState }) {
  const { account, logout } = useMoralis();
  const location = useLocation();

  let menuItems = {
    nftList: {
      name: "NFT Dashboard",
      icon: "bookOpenIcon",
      onClick: setModalState,
    },
    logout: { name: "Logout", icon: "logoutIcon", onClick: logout },
  };

  const extendedMenu = {
    score: {
      name: "Score",
      icon: "currencyDollarIcon",
      onClick: setModalState,
    },
    coinFlip: {
      name: "Coin Flip",
      icon: "switchHorizontalIcon",
      onClick: setModalState,
    },
    leaderBoard: {
      name: "Leader Board",
      icon: "presentationChartBarIcon",
      onClick: setModalState,
    },
  };
  if (["/shibakugames", "/coinsofdestiny"].indexOf(location.pathname) >= 0) {
    menuItems = {
      ...extendedMenu,
      ...menuItems,
    };
  }

  const menuComponents = Object.entries(menuItems).map(([key, value]) => (
    <MenuItem {...value} objKey={key} key={key}>
      {value.name}
    </MenuItem>
  ));

  return (
    <div className="text-white w-56 text-right fixed z-40 top-4 right-4 md:top-6 md:right-6 text-sm md:text-base">
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button className=" inline-flex justify-center w-full menu-background py-2 px-4 rounded-2xl border-2 border-opacity-50 hover:opacity-90 hover:border-opacity:80 focus:outline-none focus-visible:ring ">
            {getEllipsisTxt(account, 6)}
            <ChevronDownIcon
              className="w-5 h-5 ml-2 -mr-1 text-violet-200 hover:text-violet-100"
              aria-hidden="true"
            />
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 w-56 mt-2 origin-top-right menu-background divide-y divide-gray-100 rounded-xl shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div className="px-1 py-1 ">{menuComponents}</div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
}
