import { PlayIcon } from "@heroicons/react/outline";
import { useRef, useState } from "react";
import HashLoader from "react-spinners/HashLoader";

export function GridCard({ nft, onClick, index }) {
  const [play, setPlay] = useState(false);
  const [canPlay, setCanPlay] = useState(false);
  const videoRef = useRef();

  if (!nft.image)
    return (
      <div className="bg-white bg-opacity-25 h-[19rem] w-11/12 mx-auto animate-pulse rounded-xl"></div>
    );

  const videoVisibility = canPlay ? "visible" : "invisible w-0 h-0";
  const showImg = (play && !canPlay) || !play;

  return (
    <div
      onClick={() => onClick()}
      className="shadow-xl rounded-2xl py-5 bg-white bg-opacity-5 border-white border-opacity-10 border-2
    hover:bg-opacity-20 hover:cursor-pointer"
    >
      <div className="w-10/12 mx-auto" style={{ display: "grid" }}>
        <img
          style={{ gridColumn: 1, gridRow: 1 }}
          className={`rounded-lg ${!showImg && "invisible"}`}
          src={`https://minting.influencebackend.xyz/api/minting/profile-pic/shibakurender/${nft.token_id}`}
          alt="nft"
        />
        {play && !canPlay && (
          <div
            style={{ gridColumn: 1, gridRow: 1 }}
            className="place-self-center"
          >
            <HashLoader loading={true} size={40} color={"white"} />
          </div>
        )}
        {play && (
          <video
            style={{ gridColumn: 1, gridRow: 1 }}
            className={`${videoVisibility} rounded-lg`}
            src={nft.image}
            type="video/mp4"
            autoPlay
            muted
            loop
            width="250"
            height="250"
            onCanPlay={() => setCanPlay(true)}
            ref={videoRef}
          />
        )}
      </div>
      <div className="w-10/12 mx-auto">
        <PlayIcon
          onClick={(e) => {
            e.stopPropagation();
            setPlay(!play);
          }}
          className="h-9 w-9 text-white font-light float-right -mt-11 relative z-12  mr-3 hover:text-gray-400 hover:cursor-pointer"
        />
        <p className="text-left pl-1 pt-3 text-white text-lg text-shadow tracking-wider">
          {nft.metadata.name}
        </p>
      </div>
    </div>
  );
}
