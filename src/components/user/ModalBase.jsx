import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";

export default function ModalBase({
  children,
  closeCallback,
  isOpen,
  overflow,
  center,
  fixedHeight,
}) {
  return (
    <>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          // initialFocus={exitButtonRef}
          className="fixed inset-0 z-10"
          onClose={closeCallback}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-400"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-in duration-500"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-400"
              leaveFrom="opacity-100 scale-500"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block max-w-4xl xl:max-w-4xl px-3 md:px-6 py-4 md:py-6 my-8 overflow-hidden text-center align-middle transition-all transform menu-background shadow-xl rounded-2xl">
                <Dialog.Title
                  as="h3"
                  className="text-2xl md:text-3xl tracking-widest shadow-xl w-full rounded-2xl py-4 px-6  mx-auto leading-6 text-white border border-white border-opacity-20"
                >
                  {children[0] || "Error"}
                </Dialog.Title>
                <div
                  className={` ${
                    fixedHeight
                      ? "min-h-[73vh] max-h-[73vh]  lg:min-h-[52vh] lg:max-h-[52vh]  xl:min-h-[69vh] xl:max-h-[69vh]"
                      : // : "min-h-[73vh] max-h-[73vh]  md:min-h-0 md:max-h-[full]"
                        " max-h-[73vh]  "
                  }  
                  relative ${
                    center && "md:flex md:items-center md:justify-center"
                  } 
                  ${
                    overflow ? "overflow-y-auto" : "overflow-y-hidden"
                  } overflow-x-hidden px-2 `}
                >
                  {children[1] || "Loading"}
                </div>

                <button
                  type="button"
                  className="inline-flex tracking-widest text-lg justify-center px-12 py-2 mt-3 font-medium text-blue-90 shadow-2xl bg-white bg-opacity-10 text-white border-2 border-opacity-30 rounded-2xl hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                  onClick={() => closeCallback()}
                >
                  {children[2] || "Close"}
                </button>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
