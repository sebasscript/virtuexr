import { useVerifyMetadata } from "components/blockchain/hooks/useVerifyMetadata";
import useNFTContractBalances from "components/blockchain/hooks/useNFTContractBalances";
import ModalBase from "./ModalBase";
import { GridCard } from "./GridCard";
import { NFTDetails } from "./NFTDetails";
import { NFTList } from "./NFTList";

export default function Modals({ modalState, setModalState }) {
  // todo: show spinner if still loading
  const { data: NFTBalances } = useNFTContractBalances({
    token_address: "0xb70b759ad676b227a01f7d406e2dc9c67103aaeb",
    chain: "eth",
  });
  const { verifyMetadata } = useVerifyMetadata();

  if (!NFTBalances) return null;

  //todo: move this into NFTlist component
  const nftList = NFTBalances?.result?.map((nft, index) => {
    nft = verifyMetadata(nft);
    return (
      <GridCard
        nft={nft}
        key={nft.token_id}
        onClick={() =>
          setModalState({
            isOpen: true,
            component: "nftDetails",
            token_id: nft.token_id,
            index: index,
          })
        }
        index={index}
      />
    );
  });

  function closeModal() {
    setModalState({ isOpen: false });
  }

  const renderList = nftList?.length > 0;
  const component = modalState.component;

  // todo: render this a list or just component selective by iterating over all modal options
  // defined in config
  if (!renderList)
    return (
      <ModalBase
        isOpen={modalState.isOpen}
        nftList={nftList}
        closeCallback={closeModal}
      >
        <header>NFT Dashboard</header>
        <main className="text-white tracking-widest shadow-xl text-2xl absolute top-0 bottom-0 m-auto left-0 right-0 w-8/12 h-56 border border-white rounded-2xl flex justify-center items-center">
          No NFT Found
        </main>
        <div>Close</div>
      </ModalBase>
    );
  if (component === "nftDetails") {
    const nft = NFTBalances.result[modalState.index];
    return (
      <ModalBase
        isOpen={modalState.isOpen}
        nftList={nftList}
        closeCallback={() =>
          setModalState({
            isOpen: true,
            component: "nftList",
          })
        }
        overflow
      >
        {NFTDetails(nft)}
      </ModalBase>
    );
  }
  if (component === "nftList")
    return (
      <ModalBase
        isOpen={modalState.isOpen}
        nftList={nftList}
        closeCallback={closeModal}
        overflow
        fixedHeight
      >
        {NFTList({ nftList })}
      </ModalBase>
    );
  if (component === "nftList")
    return (
      <ModalBase
        isOpen={modalState.isOpen}
        nftList={nftList}
        closeCallback={closeModal}
        overflow
        fixedHeight
      >
        {NFTList({ nftList })}
      </ModalBase>
    );
  if (component === "score")
    return (
      <ModalBase
        isOpen={modalState.isOpen}
        nftList={nftList}
        closeCallback={closeModal}
        fixedHeight
      >
        <header>Score</header>
        <main className="text-white tracking-widest shadow-xl text-2xl absolute top-0 bottom-0 m-auto left-0 right-0 w-8/12 h-56 border border-white rounded-2xl flex justify-center items-center">
          coming soon
        </main>
        <div>Close</div>
      </ModalBase>
    );
  if (component === "leaderBoard")
    return (
      <ModalBase
        isOpen={modalState.isOpen}
        nftList={nftList}
        closeCallback={closeModal}
        fixedHeight
      >
        <header>Leader Board</header>
        <main className="text-white tracking-widest shadow-xl text-2xl absolute top-0 bottom-0 m-auto left-0 right-0 w-8/12 h-56 border border-white rounded-2xl flex justify-center items-center">
          coming soon
        </main>
        <div>Close</div>
      </ModalBase>
    );
  if (component === "coinFlip")
    return (
      <ModalBase
        isOpen={modalState.isOpen}
        nftList={nftList}
        closeCallback={closeModal}
        fixedHeight
      >
        <header>Coin Flipper</header>
        <main className="text-white tracking-widest shadow-xl text-2xl absolute top-0 bottom-0 m-auto left-0 right-0 w-8/12 h-56 border border-white rounded-2xl flex justify-center items-center">
          coming soon
        </main>
        <div>Close</div>
      </ModalBase>
    );
  return null;
}
