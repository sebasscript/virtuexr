import { Menu } from "@headlessui/react";
import {
  BookOpenIcon,
  LogoutIcon,
  CurrencyDollarIcon,
  SwitchHorizontalIcon,
  PresentationChartBarIcon,
} from "@heroicons/react/outline";

const icons = {
  bookOpenIcon: BookOpenIcon,
  logoutIcon: LogoutIcon,
  currencyDollarIcon: CurrencyDollarIcon,
  switchHorizontalIcon: SwitchHorizontalIcon,
  presentationChartBarIcon: PresentationChartBarIcon,
};

export default function MenuItem({ onClick, icon, children, objKey }) {
  if (!icons?.[icon]) {
    throw Error(`${icon} is not a valid icon`);
  }
  const UseIcon = icons[icon];

  return (
    <Menu.Item>
      {({ active }) => (
        <button
          onClick={() =>
            onClick({
              component: objKey,
              isOpen: true,
            })
          }
          className={`${
            active ? "bg-violet-500 bg-opacity-50 text-white" : "text-white"
          } group flex rounded-xl items-center w-full px-2 py-2 text-md`}
        >
          {active ? (
            <UseIcon className="h-5 w-5 mr-2" fill="#619093" stroke="white" />
          ) : (
            <UseIcon className="h-5 w-5 mr-2" fill="none" stroke="white" />
          )}
          {children}
        </button>
      )}
    </Menu.Item>
  );
}
