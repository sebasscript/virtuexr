import { useState } from "react";
import Modals from "./Modals";
import AccountMenu from "./AccountMenu";

export default function UiComponents() {
  const [isOpen, setIsOpen] = useState(false);

  const [modalState, setModalState] = useState({
    isOpen: false,
    component: null,
  });

  return (
    <>
      <AccountMenu setIsOpen={setIsOpen} setModalState={setModalState} />
      <Modals
        setIsOpen={setIsOpen}
        isOpen={isOpen}
        setModalState={setModalState}
        modalState={modalState}
      />
    </>
  );
}
