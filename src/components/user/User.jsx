import { useMoralis } from "react-moralis";
import UiComponents from "./UiComponents";
import { useIsSignedIn } from "components/blockchain/hooks/useIsSignedInd";
import { SignInButton } from "components/blockchain/SignInButton";

export default function User() {
  const { isAuthenticated } = useMoralis();
  useIsSignedIn();

  if (!isAuthenticated) return <SignInButton />;
  return <UiComponents />;
}
