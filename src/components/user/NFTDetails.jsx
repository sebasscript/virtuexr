import { PlayIcon } from "@heroicons/react/outline";
import { useRef, useState } from "react";
import HashLoader from "react-spinners/HashLoader";

export const NFTDetails = (nft) => {
  const metadata = nft?.metadata;
  const header = <header key="main">{metadata?.name}</header>;
  const button = <div key="button">Back</div>;

  const attributes = metadata?.attributes?.map((attr, index) => {
    if (index === 0) return null;
    const background =
      index % 2 === 0 ? "" : "bg-gray-500 bg-opacity-40 rounded-md";
    return (
      <div
        key={index}
        className={`grid grid-cols-2 gap-x-20 md:gap-x-24 w-11/12 mb-[0.2rem] mx-auto text-gray-100 text-left text-lg ${background} px-4 py-[0.2rem]`}
      >
        <div>{attr.trait_type}</div>
        <div>{attr.value}</div>
      </div>
    );
  });

  const main = (
    <main key="main">
      <div className="md:grid md:grid-cols-2 gap-6 py- md:py-6">
        <div className="self-stretch grid grid-cols-1 h-full ">
          <Media nft={nft} key={nft.token_id} />
          <div className="flex items-center shadow-xl rounded-2xl bg-white bg-opacity-5 border-white border-opacity-10 border-2 mt-6">
            <div>
              <p className="text-white text-center text-2xl md:text-3xl ">
                Haiku
              </p>
              <p className="h-[48%] w-9/12 md:w-9/12 lg:w-8/12 mx-auto text-gray-100 text-xl leading-loose uppercase">
                {metadata?.attributes[0].value}
              </p>
            </div>
          </div>
        </div>
        <div className="shadow-xl rounded-2xl py-3 bg-white bg-opacity-5 border-white border-opacity-10 border-2 mt-8 md:mt-0 mb-2 md:mb-0">
          <h1 className="text-white text-center text-2xl md:text-3xl mb-6">
            Attributes
          </h1>
          {attributes}
        </div>
      </div>
    </main>
  );

  const children = [header, main, button];
  return children;
};

export function Media({ nft }) {
  const [play, setPlay] = useState(false);
  const [canPlay, setCanPlay] = useState(false);
  const videoRef = useRef();

  if (!nft.image)
    return (
      <div className="bg-white bg-opacity-25 h-[19rem] w-11/12 mx-auto animate-pulse rounded-xl"></div>
    );

  const videoVisibility = canPlay ? "visible" : "invisible w-0 h-0";
  const showImg = (play && !canPlay) || !play;

  return (
    <div
      className="flex items-center shadow-xl rounded-2xl py-5 bg-white bg-opacity-5 border-white border-opacity-10 border-2
    hover:bg-opacity-20 hover:cursor-pointer"
    >
      <div>
        <div className="w-11/12 mx-auto" style={{ display: "grid" }}>
          <img
            style={{ gridColumn: 1, gridRow: 1 }}
            className={`rounded-lg ${!showImg && "invisible"}`}
            src={`https://minting.influencebackend.xyz/api/minting/profile-pic/shibakurender/${nft.token_id}`}
            alt="nft"
          />
          {play && !canPlay && (
            <div
              style={{ gridColumn: 1, gridRow: 1 }}
              className="place-self-center"
            >
              <HashLoader loading={true} size={40} color={"white"} />
            </div>
          )}
          {play && (
            <video
              style={{ gridColumn: 1, gridRow: 1 }}
              className={`${videoVisibility} rounded-lg`}
              src={nft.image}
              type="video/mp4"
              autoPlay
              muted
              loop
              width="250"
              height="250"
              onCanPlay={() => setCanPlay(true)}
              ref={videoRef}
            />
          )}
        </div>
        <div className="w-10/12 mx-auto">
          <PlayIcon
            onClick={(e) => {
              e.stopPropagation();
              setPlay(!play);
            }}
            className="h-9 w-9 text-white font-light float-right -mt-11 relative z-12  mr-3 hover:text-gray-400 hover:cursor-pointer"
          />
        </div>
      </div>
    </div>
  );
}
