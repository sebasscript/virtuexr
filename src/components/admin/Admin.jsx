// import { UserCircleIcon } from "@herocoins/react/solid";
import { Link } from "react-router-dom";
import { UserCircleIcon } from "@heroicons/react/outline";

export default function Admin() {
  return (
    <div className="w-screen h-screen">
      <div className="absolute inset-0 z-0">
        <img
          src="/Sky.jpg"
          alt="background"
          className="w-full h-full opacity-60 object-cover"
        />
      </div>
      <div className="pt-28  bg-green w-10/12 mx-auto relative z-10">
        <UserProfile />
        <ScenesGrid />
      </div>
    </div>
  );
}

function ScenesGrid() {
  return (
    <div className="grid grid-cols-4 gap-4 border-2 border-white rounded-xl py-8 px-4 border-opacity-60 mt-4">
      <Link
        to="/editor"
        className="w-44 h-44 rounded-3xl text-2xl flex justify-center items-center border border-white text-white hover:opacity-60"
      >
        +
      </Link>
    </div>
  );
}

function UserProfile() {
  return (
    <div className="border-2 border-white rounded-xl w-96 p-6 text-white text-lg border-opacity-60">
      <UserCircleIcon className="text-white h-14 w-14" />
      <p className="mt-3 bg-gray-100 bg-opacity-20 p-2 rounded-lg">
        Name: ...{" "}
      </p>
      <p className="mt-3 bg-gray-100 bg-opacity-20 p-2 rounded-lg">
        Wallet: ...{" "}
      </p>
    </div>
  );
}
