import { useMoralis } from "react-moralis";

export function SignInButton() {
  const { authenticate, isAuthenticating } = useMoralis();
  return (
    <div className="text-white absolute z-40  top-4 right-4 lg:top-6 lg:right-6">
      <button
        className=" text-sm md:text-base menu-background py-2 px-4 rounded-2xl border-2 border-opacity-60 hover:opacity-80 hover:animate-pulse hover:border-opacity-90  "
        onClick={() =>
          authenticate({
            signingMessage: "Welcome to Shibaku, please sign in to continue",
          })
        }
      >
        {isAuthenticating ? "... Connecting ..." : "Connect Wallet"}
      </button>
    </div>
  );
}
