import {
  useMoralisWeb3Api,
  useMoralis,
  useMoralisWeb3ApiCall,
} from "react-moralis";
import { useMemo } from "react";
import { resolveIPFS } from "react-moralis";

export default function useNFTContractBalances(params, options) {
  const {
    account: { getNFTsForContract },
  } = useMoralisWeb3Api();
  const { chainId, account } = useMoralis();

  //   !todo need a check if chain is supported
  const {
    fetch: getNFTContractBalance,
    data,
    error,
    isLoading,
  } = useMoralisWeb3ApiCall(
    getNFTsForContract,
    {
      chain: params?.chain ?? chainId, //?? DEFAULT_API_CHAIN,
      address: params?.address ?? account ?? "",
      token_address: params?.token_address ?? "",
      ...params,
    },
    { autoFetch: options?.autoFetch ?? !!account, ...options }
  );

  const balances = useMemo(() => {
    if (!data?.result || !data?.result.length) {
      return data;
    }

    const formattedResult = data.result.map((nft) => {
      try {
        if (nft.metadata) {
          const metadata = JSON.parse(nft.metadata);
          const image = resolveIPFS(metadata?.image);
          return { ...nft, image, metadata };
        }
      } catch (error) {
        return nft;
      }
      return nft;
    });

    return { ...data, result: formattedResult };
  }, [data]);

  return { getNFTContractBalance, data: balances, error, isLoading };
}
