import { ApiChain } from "../config/";
export const DEFAULT_API_CHAIN = "eth";
export const supportedChains = [
  "0x1",
  "0x3",
  "0x4",
  "0x5",
  "0x2a",
  "0x38",
  "0x61",
  "0x89",
  "0x13881",
  "0xfa",
  "0xa86a",
  "0xa869",
  "0x539",
];

export const isValidApiChain = (chain) => {
  if (!chain) {
    return null;
  }
  // TODO: add check if chain is in provided list
  return chain;
};
