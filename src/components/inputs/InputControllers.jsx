import { useMapStore } from "store/useMapStore";
import { useEffect } from "react";

// ! depreciated, multi store event handling caused unstable and buggy controls
// ! input components now update app state directly
export default function InputControllers({ inputs }) {
  return (
    <>
      {inputs?.joysticks && (
        <TriggerJoystickActions joystickConfigs={inputs.joysticks} />
      )}
      <TriggerKeyboardActions keyConfig={inputs?.keyboard} />
    </>
  );
}

function TriggerJoystickActions({ joystickConfigs }) {
  const { dispatch } = useMapStore(["dispatch"]);

  //joystick entities are created dynamically, create array of all keys
  const joystickKeys = Object.keys(joystickConfigs);
  // subscribe to all input eventStores
  const joysticksEvents = useMapStore(joystickKeys, "inputs");
  const { setJoystickEvents } = useMapStore(["setJoystickEvents"], "inputs");

  useEffect(() => {
    // loop through all joystick entities
    joystickKeys.forEach((joystickName) => {
      const joystickEvents = joysticksEvents[joystickName]; //single eventStore
      const joystickEvConf = joystickConfigs[joystickName].events; //events for this joystick

      if (!joystickEvents) return;
      if (!Object.keys(joystickEvents).length > 0) return;

      //loop through all events of this joystick and dispatch actions
      Object.entries(joystickEvents).forEach(([event, value]) => {
        const evenType = value.event;
        if (joystickEvConf[evenType].events[event]) {
          const actions = joystickEvConf[evenType].events[event].actions;
          if (actions.length > 0) dispatch({ actions });
        }
      });
      // clear event list
      setJoystickEvents(joystickName, {});
    });
  }, [
    joysticksEvents,
    joystickConfigs,
    dispatch,
    joystickKeys,
    setJoystickEvents,
  ]);
  return null;
}

function TriggerKeyboardActions({ keyConfig }) {
  const keyConfEvents = keyConfig.events;
  const { keyboardEvents, setKeyboardEvents } = useMapStore(
    ["keyboardEvents", "setKeyboardEvents"],
    "inputs"
  );
  const { dispatch } = useMapStore(["dispatch"]);

  useEffect(() => {
    if (!Object.keys(keyboardEvents).length > 0) return;

    Object.entries(keyboardEvents).forEach(([key, value]) => {
      const eventType = value.event.type;
      // key event in events config?
      if (keyConfEvents[eventType]?.events[key]) {
        // dispatch config event actions
        const actions = keyConfEvents[eventType].events[key].actions;
        if (actions.length > 0)
          dispatch({
            actions,
          });
      }
    });
    setKeyboardEvents({});
  }, [keyboardEvents, keyConfEvents, dispatch, setKeyboardEvents]);
  return null;
}
