import useKeyboardInput from "./useKeyboardInput";
import Joystick from "./Joystick";

export default function Inputs({ inputs, device }) {
  const { joystickConf, keyboardConf } = {
    //todo: one passes events other one entity object,
    joystickConf: inputs?.joysticks || false,
    keyboardConf: inputs?.keyboard?.events || false,
  };

  return (
    <>
      {keyboardConf && <Keyboard keyboardConf={keyboardConf} />}
      {joystickConf && device.isTouch && <Joysticks joysticks={joystickConf} />}
    </>
  );
}

function Joysticks({ joysticks }) {
  const components = Object.entries(joysticks).map(([key, value]) => {
    if (value.active) return <Joystick key={key} name={key} {...value} />;
    return null;
  });
  return <>{components}</>;
}

function Keyboard(keyboardConf) {
  useKeyboardInput(keyboardConf);
  return null;
}
