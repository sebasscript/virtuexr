import { useEffect, useRef } from "react";
import { useMapStore } from "store/useMapStore";
import produce from "immer";

export default function useKeyboardInput(keyConfEvents) {
  console.log(keyConfEvents);
  const { dispatch } = useMapStore(["dispatch"]);

  // useRef for local state to avoid rerenders
  const keyEvents = useRef({});

  useEffect(() => {
    const handleKeyDown = (event) => {
      const { key, type } = event;

      // skip active held keys
      if (keyEvents.current[key]) return;

      // key event in events config -> dispatch?
      if (keyConfEvents[type]?.events[key]) {
        // dispatch config event actions
        const actions = keyConfEvents[type].events[key].actions;
        if (actions.length > 0)
          dispatch({
            actions,
          });
      }
      // update local state
      keyEvents.current = produce(keyEvents.current, (draft) => {
        draft[key] = {
          event,
        };
      });
    };

    const handleKeyUp = (event) => {
      const { key, type } = event;

      // key event in events config -> dispatch?
      if (keyConfEvents[type]?.events[key]) {
        // dispatch config event actions
        const actions = keyConfEvents[type].events[key].actions;
        if (actions.length > 0)
          dispatch({
            actions,
          });
      }

      // remove from local state
      keyEvents.current = produce(keyEvents.current, (draft) => {
        delete draft[key];
      });
    };

    // register listeners
    window.addEventListener("keydown", handleKeyDown);
    window.addEventListener("keyup", handleKeyUp);

    // cleanup
    return () => {
      window.removeEventListener("keydown", handleKeyDown);
      window.removeEventListener("keyup", handleKeyUp);
    };
    // eslint-disable-next-line
  }, []);
}
