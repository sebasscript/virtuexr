import { useRef, useEffect } from "react";
import { useMapStore } from "store/useMapStore";
import nipplejs from "nipplejs";
import produce from "immer";

export default function Joystick({
  config,
  active,
  events,
  name = "joystick",
}) {
  const { position = "left" } = config;
  const joyRef = useRef();
  const joyStateRef = useRef({});
  const joystick = useRef(null);

  const { dispatch } = useMapStore(["dispatch"]);

  useEffect(() => {
    if (joystick.current) return;
    joystick.current = nipplejs.create({
      zone: joyRef.current,
      color: "white",
      mode: "static",
      position: {
        top: "50%",
        left: "50%",
      },
    });

    // todo: try to move functions outside of useEffect
    const updateState = (stateRef, dir, eventType) => {
      // update global event store
      //loop through all events of this joystick and dispatch actions
      if (events[eventType].events[dir]) {
        const actions = events[eventType].events[dir].actions;
        if (actions.length > 0) dispatch({ actions });
      }

      if (eventType === "end") {
        // remove from local ref state if event ended
        const updateState = produce(stateRef.current, (draft) => {
          delete draft[dir];
        });
        stateRef.current = updateState;
      } else {
        // add to local ref state
        stateRef.current = produce(stateRef.current, (draft) => {
          draft[dir] = {
            eventType,
          };
        });
      }
    };

    // todo: add threshold to config
    const updateDir = (vectorComp, stateRef, dirs, threshold) => {
      // Activate
      if (vectorComp > threshold && !stateRef.current[dirs.posDir]) {
        updateState(stateRef, dirs.posDir, "start");
      }
      if (vectorComp < -threshold && !stateRef.current[dirs.negDir]) {
        updateState(stateRef, dirs.negDir, "start");
      }
      // Release
      if (vectorComp < threshold && stateRef.current[dirs.posDir]) {
        updateState(stateRef, dirs.posDir, "end");
      }
      if (vectorComp > -threshold && stateRef.current[dirs.negDir]) {
        updateState(stateRef, dirs.negDir, "end");
      }
    };

    function onEnd(stateRef) {
      // todo: could improve this to unbundle the arrays and push updates with one dispatch
      const activeDirections = Object.keys(stateRef.current);
      if (activeDirections.length === 0) return;
      activeDirections.forEach((dir) => {
        const actions = events?.end?.events[dir].actions;
        if (actions.length > 0) dispatch({ actions });
      });
    }

    joystick.current
      .on("move", function (evt, nipple) {
        updateDir(
          nipple.vector.y,
          joyStateRef,
          { posDir: "forward", negDir: "backward" },
          0.25,
          "move"
        );
        updateDir(
          nipple.vector.x,
          joyStateRef,
          { posDir: "right", negDir: "left" },
          0.25,
          "move"
        );
      })
      .on("end", function (evt, nipple) {
        // update global store
        onEnd(joyStateRef);
        // reset local tracking state
        joyStateRef.current = {};
      })
      .on("removed", function (evt, nipple) {
        nipple.off("start move end dir plain");
      });

    return () => {
      joystick.current.destroy();
    };
    // eslint-disable-next-line
  }, []);

  // todo: can remove active check and only do css size detection
  // const isActive = active ? "inline xl:hidden" : "hidden";

  if (position === "left")
    return (
      <div className={`absolute bottom-16 left-16 z-40`} ref={joyRef}></div>
    );
  if (position === "right")
    return (
      <div className={`absolute bottom-16 right-16 z-40`} ref={joyRef}></div>
    );
}
