import { useGLTF } from "@react-three/drei";

export default function Stage() {
  const gltf = useGLTF("/stage/stage.gltf", "/draco/");
  // const gltf = useGLTF("/shibakudoor/shibakudoor.gltf");
  return (
    <primitive
      object={gltf.scene}
      scale={0.05}
      position={[-0.018, -0.1, 0.06]}
      rotation={[0, -Math.PI / 1.97, 0]}
    />
  );
}
