import { Canvas, useFrame } from "@react-three/fiber";
import { BackSide, TextureLoader } from "three";
import { useLoader } from "@react-three/fiber";
import { Suspense, useRef } from "react";
import { Link } from "react-router-dom";

export default function Landing() {
  return (
    <div className="relative">
      <Content />
      <LandingCanvas />
    </div>
  );
}

function LandingCanvas() {
  return (
    <div className="w-screen h-screen z-0 absolute left-0 top-0">
      <Canvas>
        <ambientLight />
        <Suspense fallback="">
          <Skydome />
        </Suspense>
      </Canvas>
    </div>
  );
}

function Content() {
  return (
    <div className="flex items-center justify-center h-screen w-screen  mx-auto text-center absolute top-0 left-0 z-10">
      <div>
        <h1 className=" text-4xl md:text-6xl xl:text-8xl  font-mrrobot animate-gradient-x bg-gradient-to-r from-white via-blue-100 to-blue-400 text-transparent bg-clip-text px-2">
          Virtue Xr
        </h1>
        <p className="text-2xl xl:text-4xl text-white">Metaversal Toolkit</p>

        <Link
          className="bg-white hover:bg-opacity-40 text-white px-16 py-[7px] bg-opacity-20   backdrop-filter backdrop-blur rounded-3xl mt-3 text-xl  inline-block"
          to="/preview"
        >
          Enter
        </Link>
      </div>
    </div>
  );
}

function Skydome() {
  const ref = useRef();
  const ref2 = useRef();
  const texture = useLoader(TextureLoader, "./sky.jpg");

  useFrame((state) => {
    ref.current.rotation.x += 0.0006;
    ref.current.rotation.y += 0.0006;
    ref2.current.rotation.x += 0.0009;
    ref2.current.rotation.y += 0.0009;
  });

  return (
    <Suspense fallback="">
      <mesh ref={ref}>
        <sphereGeometry args={[15, 32, 16]} />
        <meshBasicMaterial side={BackSide} map={texture} />
      </mesh>
      <mesh position={[0, 0, 0]} ref={ref2}>
        <sphereGeometry args={[2.4, 32, 16]} />
        <meshBasicMaterial wireframe={true} color="#4a4641" />
      </mesh>
    </Suspense>
  );
}
