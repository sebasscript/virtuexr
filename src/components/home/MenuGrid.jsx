import { useState } from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { MenuGridRoutes } from "routes.js";
// import { useMapStore } from "store/useMapStore";

function MenuGrid() {
  const { pathname } = useLocation();
  // const { menuGridsItems } = useMapStore(["menuGridsItems"]);
  const gridItems = MenuGridRoutes[pathname];
  const list = gridItems?.map((item) => <MenuItem {...item} key={item.path} />);

  return (
    <div className="grid pt-14 lg:pt-0 md:grid-cols-2 gap-y-6 md:gap-y-16 lg:gap-y-11 xl:gap-y-8 justify-items-center md:w-11/12  lg:w-10/12 mx-auto">
      {list}
    </div>
  );
}
export default MenuGrid;

function MenuItem({ imgPath, path, name }) {
  const [hover, setHover] = useState(false);
  return (
    <Link
      className="relative"
      to={path}
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <div
        className={`bg-gradient-to-b from-white to-[#E68285] p-[5px] rounded-2xl ${
          hover && "opacity-40"
        } shadow-white`}
      >
        <img
          src={imgPath}
          key={path}
          alt={name}
          className="w-72 md:w-[30rem] lg:w-[35rem] xl:w-[24rem] 2xl:w-[30rem] rounded-2xl"
        />
      </div>
      {hover && (
        <p className="absolute text-xl xl:text-3xl top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 font-serif text-white text-center">
          {name}
        </p>
      )}
    </Link>
  );
}
