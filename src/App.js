import { Route, Switch } from "react-router-dom";
import { routes } from "routes";

import {
  computeBoundsTree,
  disposeBoundsTree,
  acceleratedRaycast,
} from "three-mesh-bvh";
import { BufferGeometry, Mesh } from "three";
// import User from "components/user/User";

// add mesh-bvh
BufferGeometry.prototype.computeBoundsTree = computeBoundsTree;
BufferGeometry.prototype.disposeBoundsTree = disposeBoundsTree;
Mesh.prototype.raycast = acceleratedRaycast;

const list = Object.values(routes).map((route) => {
  return <Route {...route} key={route.path} />;
});

export default function App() {
  return (
    <>
      {/* <User /> */}
      <Switch>{list}</Switch>
    </>
  );
}
