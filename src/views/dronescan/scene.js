export const scene = {
  camera: {
    config: {
      position: {
        // default: [0, 20, 20],
        default: [0, -10, -80],
        mobile: [0, 0, 20],
        uiControls: true,
      },
      near: 1,
      far: 1000,
      fov: 70,
      // target: [0, 42, 0],
      // todo: ui controls stopped working
    },
  },
  render: {
    config: {
      toneMappingExposure: 0.8,
      frameloop: "always",
      // toneMapping: "LinearToneMapping",
      // frameloop: "demand",
    },
  },
  controllers: {
    orbitController: {
      active: true,
      config: {
        // enableZoom: false,
        makeDefault: true,
        target: {
          default: [0, -40, 0],
          mobile: [0, 0, 0],
        },
        // enablePan: false,
        // maxAzimuthAngle: Math.PI / 6.2,
        // minAzimuthAngle: -Math.PI / 6.2,
        // maxPolarAngle: Math.PI / 1.8,
        // minPolarAngle: Math.PI / 2.8,
      },
    },
    // fpsController: {
    //   active: false,
    // },
    transformController: {
      active: true,
      config: {
        logUpdates: false,
      },
    },
  },
  entities: {
    preloader: {
      config: {
        backgroundImg: "/images/holodeck.jpeg",
        img: "",
        text: "<p style='text-shadow:2px 2px 6px #FFFFFF' className='text-5xl'>Loading Editor<p>",
      },
    },
    gltfs: {
      dome: {
        config: {
          path: "gltf/dronescan/untitled.gltf",
          position: [0, 0, 0],
          scale: 1,
          enableTransforms: false,
        },
      },
    },
    threeEntities: {
      environment: {
        type: "environment",
        config: {
          path: "/hdr/",
          files: "environment.hdr",
        },
      },
      // baseGrid: {
      //   type: "grid",
      //   config: {
      //     size: 100,
      //     divisions: 100,
      //   },
      // },
      skyShader: {
        type: "skyShader",
      },
      pointLight: {
        type: "pointLight",
        config: {
          position: [200, 200, 200],
          intensity: 0.8,
        },
      },
      // ambientLight: {
      //   type: "ambientLight",
      //   config: {
      //     intensity: 0.8,
      //   },
      // },
      // baseGrid: {
      //   type: "plane",
      // },
    },
  },
  events: {
    afterPreloader: {
      actions: [],
    },
  },
};
