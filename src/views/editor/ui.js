export const ui = {
  menu: {
    main: {
      config: {
        frame: false,
        position: "topLeft",
      },
      entities: {
        uploadGltf: {
          active: true,
          config: {
            type: "button",
            name: "Upload Model",
          },
        },
      },
      events: {
        onClick: {
          actions: [],
        },
      },
    },
  },
};
