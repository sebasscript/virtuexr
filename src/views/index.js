import { editor } from "./editor";
import { dome } from "./dome";
import { floattank } from "./floattank";
import { emplant } from "./emplant";
import { ebs } from "./ebs";
import { bioarc } from "./bioarc";
import { dronescan } from "./dronescan";
import { floattankscan } from "./floattankscan";
import { stardome } from "./stardome";

export {
  editor,
  dome,
  floattank,
  emplant,
  ebs,
  bioarc,
  dronescan,
  floattankscan,
  stardome,
};
// this is imported into useSetActiveView which performs a route match
// that loads the view config and writes into to the viewsStore
