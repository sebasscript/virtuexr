import { scene } from "./scene";
import { audio } from "./audio";
import { inputs } from "./inputs";
import { ui } from "./ui";

export const floattankscan = {
  scene,
  inputs,
  ui,
  audio,
};
