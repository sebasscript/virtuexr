export const scene = {
  camera: {
    config: {
      position: {
        default: [0, 51, 188],
        mobile: [0, 81, 235],
      },
      near: 1,
      far: 10000,
      fov: 70,
      // target: [0, 42, 0],
      uiControls: false,
    },
  },
  render: {
    config: {
      toneMappingExposure: 0.9,
      frameloop: "always",
      // toneMapping: "LinearToneMapping",
      // frameloop: "demand",
    },
  },
  controllers: {
    // postprocessingController: {
    //   active: true,
    //   config: {
    //     intensity: 1,
    //     threshold: 0.4,
    //     smooth: 1.2,
    //   },
    // },
    orbitController: {
      active: true,
      config: {
        enableZoom: false,
        enablePan: false,
        maxAzimuthAngle: Math.PI / 6.2,
        minAzimuthAngle: -Math.PI / 6.2,
        maxPolarAngle: Math.PI / 1.8,
        minPolarAngle: Math.PI / 2.8,
        target: {
          default: [0, 50, 50],
          mobile: [0, 65, 0],
        },
      },
    },
  },
  entities: {
    preloader: {
      config: {
        backgroundImg: "/images/door.jpg",
        img: "/images/shibakulogo.png",
        // text: "<p style='text-shadow:2px 2px 6px #FFFFFF'>... welcome to the Shibaku <br/> metaverse preview ...<p>",
        text: "<p style='text-shadow:2px 2px 6px #FFFFFF' className='text-5xl'>...Metaverse...<p>",
        // autoClose: false,
      },
    },
    gltfs: {
      avatar: {
        config: {
          path: "/avatar/shibaku.glb",
          scale: 100,
          position: [0, -8.5, -8],
          playAnimations: ["idle"],
        },
      },
      shibakudoor: {
        config: {
          path: "/shibaverse/shibaverse.gltf",
          modelName: "shibakudoor",
          playAnimations: [
            "1_3.066Action",
            // "wheelAction",
            // "Cylinder_1Action",
            // "glassCover rotation",
            // "handle.001Action",
          ],
        },
      },
    },
    threeEntities: {
      pointLight: {
        type: "pointLight",
        config: {
          position: [200, 200, 200],
          intensity: 0.4,
        },
      },
      environment: {
        type: "environment",
        config: {
          path: "/hdr/",
          files: "environment.hdr",
        },
      },
    },
  },
  events: {
    afterPreloader: {
      actions: [
        {
          target: "modal",
          payload: {
            active: true,
            title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Welcome</p>`,
            body: `<div className="my-5 flex justify-center">
                    <div className="grid grid-cols-12 gap-y-2">
                      <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                        1
                      </div>
                      <div className="col-span-10 ml-3 self-center ">
                        Click and drag to look around
                      </div>
                      <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                        2
                      </div>
                      <div className="col-span-10 ml-3 self-center">
                        Use the menu to explore the preview
                      </div>
                      <div className="col-span-2 bg-white bg-opacity-30 text-center p-2 rounded-full w-10 h-10 align justify-self-end">
                      3
                    </div>
                    <div className="col-span-10 ml-3 self-center">
                      Press the "Music" button to enable the music
                    </div>
                    </div>
                  </div>`,
          },
        },
      ],
    },
  },
};
