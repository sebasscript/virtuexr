export const audio = {
  // todo: ?? toggle audio on off by mutation active state of these entities
  backgroundDefault: {
    config: {
      url: "/audio/backgroundDefault.mp3",
      loop: true,
      volume: 0.2,
    },
  },
  jump: {
    config: {
      url: "/audio/jump.mp3",
    },
  },
  shibaku: {
    config: {
      url: "/audio/shibaku.mp3",
      useOnce: true,
    },
  },
  lever: {
    config: {
      url: "/audio/lever.mp3",
    },
  },
  camera: {
    config: {
      url: "/audio/camera.mp3",
      interrupt: true,
      volume: 0.7,
    },
  },
  wheel: {
    config: {
      url: "/audio/wheel.mp3",
      playTillEnd: true,
      volume: 0.2,
    },
  },
};
