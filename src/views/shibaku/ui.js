export const ui = {
  ActionButtons: {
    config: {
      position: "bottomCenter",
      name: "",
    },
    entities: {
      spinWheel: {
        active: false,
        config: {
          type: "button",
          name: "Spin The Wheel",
          // todo: better way for config and formatting {format: "large", "round" etc}
          large: true,
        },
        events: {
          onClick: {
            actions: [
              {
                dispatch: "setActiveModelAnim",
                config: {
                  modelName: "shibakudoor",
                  animation: "wheelAction",
                },
                payload: {
                  // todo: start at stopped working??
                  startAt: 1.5,
                  loop: false,
                  active: true,
                },
              },
              {
                dispatch: "setActiveModelAnim",
                config: {
                  modelName: "shibakudoor",
                  animation: "handle anim meAction",
                },
                payload: {
                  loop: false,
                  active: true,
                },
              },
              {
                dispatch: "setActiveAudio",
                payload: {
                  name: "lever",
                },
              },
              {
                dispatch: "setActiveAudio",
                payload: {
                  name: "wheel",
                },
              },
            ],
          },
        },
      },
      jump: {
        active: false,
        config: {
          type: "button",
          name: "Shibaku",
          active: false,
          large: true,
        },
        events: {
          onClick: {
            actions: [
              {
                dispatch: "setActiveModelAnim",
                config: {
                  modelName: "avatar",
                  animation: "jump",
                },
                payload: {
                  loop: false,
                  active: true,
                },
              },
              {
                dispatch: "setActiveAudio",
                payload: {
                  name: "jump",
                },
              },
              {
                dispatch: "setActiveAudio",
                config: {
                  useOnce: true,
                },
                payload: {
                  name: "shibaku",
                },
              },
            ],
          },
        },
      },
    },
  },
  Menu: {
    config: {
      name: "Menu",
      position: "topLeft",
      frame: true,
      groupName: true,
    },
    entities: {
      welcome: {
        active: true,
        config: {
          type: "button",
          name: "Welcome",
          icon: "",
        },
        events: {
          onClick: {
            actions: [
              {
                target: "cameraTween",
                config: {
                  stateGroup: "default",
                },
                payload: {
                  tween: true,
                  duration: 1350,
                  active: true,
                  position: {
                    default: [0, 51, 188],
                    mobile: [0, 81, 235],
                  },
                },
              },
              {
                target: "cameraTargetTween",
                payload: {
                  active: true,
                  duration: 1350,
                  tween: true,
                  target: {
                    default: [0, 50, 0],
                    mobile: [0, 65, 0],
                  },
                },
              },
              {
                dispatch: "setActiveAudio",
                payload: {
                  // todo: this should require active prop
                  name: "camera",
                },
              },
            ],
          },
        },
      },
      haikuView: {
        active: true,
        config: {
          type: "button",
          name: "Haikus",
          icon: "",
        },
        events: {
          onClick: {
            config: {
              name: "haikuView",
            },
            actions: [
              {
                target: "cameraTargetTween",
                config: {
                  stateGroup: "default",
                },
                payload: {
                  active: true,
                  duration: 1350,
                  tween: true,
                  target: {
                    default: [0, 55, 0],
                    mobile: [0, 55, 0],
                  },
                },
              },
              {
                dispatch: "setActiveAudio",
                payload: {
                  active: true,
                  name: "camera",
                },
              },
              {
                target: "cameraTween",
                payload: {
                  active: true,
                  duration: 1350,
                  tween: true,
                  position: {
                    default: [0, 55, 80],
                    mobile: [0, 55, 150],
                  },
                },
                onComplete: [
                  {
                    target: "modal",
                    config: {
                      useOnce: true,
                    },
                    payload: {
                      active: true,
                      title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Words Of Wisdom</p>`,
                      body: `
                          <div className="my-5 flex justify-center leading-normal md:leading-relaxed">
                            <div className="text-base md:text-xl text-center leading-relaxed">
                              Coming soon, listen <br/> to the "Dog Whisperer" soothing <br/>
                              your mind with Haikus of wisdom!
                            </div>
                          </div>
                        `,
                    },
                  },
                ],
              },
            ],
          },
        },
      },
      coinsView: {
        active: true,
        config: {
          type: "button",
          name: "Coin Flipper",
          icon: "",
        },
        events: {
          onClick: {
            actions: [
              {
                target: "cameraTween",
                config: {
                  stateGroup: "coins",
                },
                payload: {
                  active: true,
                  duration: 1350,
                  tween: true,
                  position: {
                    default: [0, 16, 145],
                    mobile: [0, 30, 240],
                  },
                },
                onComplete: [
                  {
                    target: "modal",
                    config: {
                      useOnce: true,
                    },
                    payload: {
                      active: true,
                      title: `<p className="text-3xl md:text-4xl text-center"  style='text-shadow:4px 4px 4px #FFFFFF'>Coins Of Destiny</p>`,
                      body: `
                                  <div className="my-5 flex justify-center leading-normal md:leading-relaxed">
                                    <div className="text-base md:text-xl text-center">
                                      Coming Q1 2022, <br/> play the "Coins Of Destiny"<br/>
                                      to win awesome prizes  and rewards!<br/>
                                      Click the "Spin The Wheel" button to try!
                                    </div>
                                  </div>
                                  `,
                    },
                  },
                  {
                    dispatch: "setUpdateView",
                    config: {
                      stateGroup: "coins",
                      name: "activateWheelButton",
                      targetPath: "ui.ActionButtons.entities.spinWheel",
                    },
                    // todo: way to organize data structure for viewStore for root, config, state?
                    // add config that defines how payload is passed into setter
                    payload: {
                      active: true,
                    },
                    payloadCache: {
                      active: false,
                    },
                    // todo: read initial/previous state from store by default?
                    // if payloadCache is set read from default state
                    // with this default state can be updated based on interactions
                    // but no default state ise set for simple group on off behavior from viewStore default
                  },
                ],
              },
              {
                target: "cameraTargetTween",
                payload: {
                  active: true,
                  duration: 1350,
                  tween: true,
                  target: {
                    default: [0, 16, 0],
                    mobile: [0, 50, 0],
                  },
                },
              },
              {
                dispatch: "setActiveAudio",
                payload: {
                  name: "camera",
                },
              },
            ],
          },
        },
      },
      avatarView: {
        active: true,
        config: {
          name: "Avatar",
          icon: "",
          type: "button",
        },
        events: {
          onClick: {
            actions: [
              {
                target: "cameraTargetTween",
                config: {
                  stateGroup: "avatar",
                },
                payload: {
                  active: true,
                  duration: 1350,
                  tween: true,
                  target: {
                    default: [0, 5.5, 0],
                    mobile: [0, 5, 0],
                  },
                },
              },
              {
                dispatch: "setActiveAudio",
                payload: {
                  name: "camera",
                },
              },
              {
                target: "cameraTween",
                payload: {
                  active: true,
                  duration: 1350,
                  tween: true,
                  position: {
                    default: [0, 6, 21],
                    mobile: [0, 6, 45],
                  },
                },
                onComplete: [
                  {
                    target: "modal",
                    config: {
                      useOnce: true,
                      stateGroup: "avatar",
                    },
                    payload: {
                      active: true,
                      title: `<p className="text-3xl md:text-4xl text-center" style='text-shadow:4px 4px 4px #FFFFFF'>Find The Elders</p>`,
                      body: `
                                  <div className="my-5 flex justify-center ">
                                    <div className="text-base md:text-xl text-center leading-relaxed">
                                      Coming 2022, <br/> play our RPG Game 
                                      and help <br/> the Shibakus to find The Elders!
                                    </div>
                                  </div>
                                `,
                    },
                  },
                  {
                    dispatch: "setUpdateView",
                    config: {
                      stateGroup: "avatar",
                      name: "activateAvatarButton",
                      type: "toggle",
                      targetPath: "ui.ActionButtons.entities.jump",
                    },
                    payload: {
                      active: true,
                    },
                    payloadCache: {
                      active: false,
                    },
                  },
                ],
              },
            ],
          },
        },
      },
      music: {
        active: true,
        config: {
          type: "button",
          name: "Music",
          icon: "",
        },
        events: {
          onClick: {
            actions: [
              {
                dispatch: "setActiveAudio",
                payload: {
                  active: true,
                  name: "backgroundDefault",
                },
              },
            ],
          },
        },
      },
    },
  },
};
