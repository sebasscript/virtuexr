import shallow from "zustand/shallow";
import { useRef, useEffect } from "react";
import { config } from "./mapStore.config.js";

// ** map store generator
function createMapStore(storeName, props) {
  const storeSelection = {};
  props.forEach((prop) => {
    const state = storeName((state) => state[prop]);
    storeSelection[prop] = state;
    if (state === undefined && config.warOnUndefined)
      console.warn(`warning from useMapStore: state ${prop} is undefined`);
  });
  return storeSelection;
}

function createMapStoreArray(storeName, props) {
  return storeName(
    (state) =>
      props.map((prop) => {
        if (state[prop] === undefined && config.warOnUndefined)
          console.warn(
            `warning from useMapStoreArr: state ${prop} is undefined`
          );
        return state[prop];
      }),
    shallow
  );
}
// todo: merge with top functions, don't need to create multiple function anymore
// **map store functions
export function useMapStore([...props], store = "default") {
  // todo:  add error catch for stores[store] doesn't exist
  return createMapStore(config.stores[store], props);
}
// todo: adapt to new syntax
export function useMapStoreArr(...props) {
  return createMapStoreArray(config.stores.default, props);
}

export function useSubscribe(selector, storeName = "default") {
  // todo: use selector middleware for subscribe
  const store = config.stores[storeName];
  const stateRef = useRef(store.getState()[selector]);
  //todo: const initialRender ? should run only once?

  useEffect(() => {
    const subscription = store.subscribe(
      (state) => (stateRef.current = state[selector])
    );
    return () => subscription();
  }, [selector, store]);

  return stateRef.current;
}
