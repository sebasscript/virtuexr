import produce from "immer";

export const viewsStore = (set, get) => ({
  // todo: view is not optional, remove or keep?
  updateView: (view, key, stateUpdate) =>
    set(
      produce((state) => {
        state.activeView[key] = stateUpdate;
      })
    ),
  activeView: null,
  setActiveView: (viewData) =>
    set({ activeView: viewData }, false, "setActiveView"),
});
