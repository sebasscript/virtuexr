import { useStore } from "./useStore";
import { useInputsStore } from "./useInputsStore";
// import { useHaikuStore } from "./useHaikuStore";

export const config = {
  stores: {
    default: useStore,
    inputs: useInputsStore,
    // haiku: useHaikuStore,
  },
  // todo: fix spelling
  warOnUndefined: true,
};
