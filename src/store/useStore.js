import create from "zustand";
import { devtools } from "zustand/middleware";
import { appStore } from "./appStore";
import { viewsStore } from "./viewsStore";
import { cloneDeep } from "lodash";

// todo: merge this with appStore
export const useStore = create(
  devtools(
    (set, get) => ({
      //todo: move preloader into appStore
      ...viewsStore(set, get),
      preloader: null,
      setPreloader: (preloader) => set({ preloader }),
      ...appStore(set, get),
    }),
    { name: "Store" }
  )
);

const initState = cloneDeep(useStore.getState());
export function resetStore() {
  useStore.setState(initState);
}
