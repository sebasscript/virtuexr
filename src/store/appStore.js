import produce from "immer";
import { get as getLo } from "lodash";

export const appStore = (set, get) => ({
  camera: {},
  modal: {},
  cameraTween: {},
  cameraTargetTween: {},
  activeAudio: {},
  stateGroup: "default",
  stateGroupCache: [],
  activeModelAnim: {},
  fpsController: {
    forward: false,
    backward: false,
    left: false,
    right: false,
    jump: false,
    rotateLeft: false,
    rotateRight: false,
    rotateUp: false,
    rotateDown: false,
  },
  transFormController: {
    active: false,
    name: null,
    mode: "translate",
  },
  device: {},

  dispatch: ({ actions, path, name }) =>
    set(
      (state) => {
        actions.forEach((action, actionIndex) => {
          if (!action?.disable) {
            // todo: refactor dispach and set section in one function
            const actionPath = path + `[${actionIndex}]`;
            // set action target state
            if (action?.target) {
              set(
                {
                  [action.target]: {
                    config: action.config,
                    state: action.payload,
                    onComplete: action.onComplete,
                    path: actionPath,
                  },
                },
                false,
                `set ${action.target}`
              );
              // dispatch action setter function
            } else if (action?.dispatch) {
              state[action.dispatch]({
                ...action,
                path: actionPath,
              });
            }

            // disable single use action
            if (action?.config?.useOnce) {
              state.setUpdateView({
                action,
                path: actionPath,
                mutation: { disable: true },
              });
            }

            // stateGroup
            const stateGr = action.config?.stateGroup;
            if (stateGr) {
              if (!(stateGr === state.stateGroup)) {
                //  update group name, reset cache
                set({ stateGroup: stateGr }, false, "setStateGroup");
                if (state.stateGroupCache.length > 0) state.resetStateGroup();
              }
              if (action?.payloadCache && !action.config.useOnce) {
                // cache the action
                set(
                  produce((state) => {
                    state.stateGroupCache.push({ action, actionPath });
                  }),
                  false,
                  "setStateGroupCache"
                );
              }
            }
          }
        });
      },
      false,
      `dispatch ${name}`
    ),

  setUpdateView: ({ config, payload, path, mutation }) =>
    set(
      produce((state) => {
        const activeView = state[config?.view || "activeView"];
        const targetPath = config?.targetPath || path;
        const entity = getLo(activeView, targetPath);
        // todo: better payload targeting?
        // payload is dumped into root only at the moment, need extension?
        // idea add config option that defines if payload is passed as root or into config, payload etc.
        if (mutation) {
          Object.entries(mutation).forEach(([key, value]) => {
            entity[key] = value;
          });
        } else {
          Object.entries(payload).forEach(([key, value]) => {
            entity[key] = value;
          });
        }
      }),
      false,
      "updateView"
    ),

  resetStateGroup: () =>
    set(
      (state) => {
        state.stateGroupCache.forEach((item) => {
          const { action, actionPath } = item;
          // set action target state
          // todo: reuse refactored setter and dispatch function
          if (action?.target) {
            set(
              {
                [action.target]: {
                  config: action.config,
                  state: action.payloadCache,
                  onComplete: action.onComplete,
                  path: actionPath,
                },
              },
              false,
              `set ${action.target}`
            );
            // dispatch action setter function
          } else if (action?.dispatch) {
            state[action.dispatch]({
              config: action.config,
              payload: action.payloadCache,
              onComplete: action.onComplete,
              path: actionPath,
            });
          }
        });
        set({ stateGroupCache: [] });
      },
      false,
      "resetStateGroup"
    ),

  //# try to abstract these into reducer types and update state inside of dispatch
  // todo: possible to generalize these setter?
  setActiveAudio: ({ payload }) =>
    set(
      produce((draft) => {
        // remove item if already active
        if (payload.name in draft.activeAudio) {
          delete draft.activeAudio[payload.name];
          return;
        }
        draft.activeAudio[payload.name] = { ...payload };
      }),
      false,
      "setActiveAudio"
    ),

  setActiveModelAnim: ({ payload, config: { animation, modelName } }) => {
    set(
      produce((state) => {
        if (!state.activeModelAnim[modelName])
          state.activeModelAnim[modelName] = {};
        state.activeModelAnim[modelName][animation] = {
          ...state.activeModelAnim[modelName][animation],
          ...payload,
        };
      }),
      false,
      "updateActiveModelAnim"
    );
  },

  // ###### still need to integrate into dispatch globaly
  // #### still need refactor / broken
  setFpsController: ({ payload }) => {
    set(
      produce((state) => {
        //todo: update directly the direction and use rotateX: -1 or 1 for direction
        // refactor to general action prop active: false/true, as well as direction: jump, left, right etc.
        state.fpsController[payload.direction] = payload.active;
      })
    );
  },

  setTransFormController: (newState) =>
    set((state) => {
      const stateUpdate = { ...state.transFormController, ...newState };
      set({
        transFormController: stateUpdate,
      });
    }),
});
