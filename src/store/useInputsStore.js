import create from "zustand";
import { devtools } from "zustand/middleware";

// ! depreciated multi store caused unstable controls
export const useInputsStore = create(
  devtools(
    (set, get) => ({
      keyboardEvents: {},
      setKeyboardEvents: (keyboardEvents) => set({ keyboardEvents }),

      joystickEvents: {},
      setJoystickEvents: (stateName, events) =>
        set({ [stateName]: { ...events } }),
    }),
    { name: "Inputs" }
  )
);
