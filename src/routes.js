import Landing from "components/home/Landing";
import Admin from "components/admin/Admin";
import Main from "components/vxr";
import Home from "components/home/Home";

export const routes = {
  home: {
    path: "/",
    exact: true,
    component: Landing,
  },
  admin: {
    path: "/admin",
    exact: true,
    component: Admin,
  },
  // viewer: {
  //   path: "/editor",
  //   exact: true,
  //   component: Main,
  // },
  ebs: {
    path: "/ebs",
    exact: true,
    component: Main,
  },
  preview: {
    path: "/preview",
    exact: true,
    component: Home,
  },
  floatTank: {
    path: "/floattank",
    exact: true,
    component: Main,
  },
  floatTankScan: {
    path: "/floattankscan",
    exact: true,
    component: Main,
  },
  dome: {
    path: "/dome",
    exact: true,
    component: Main,
  },
  stardome: {
    path: "/stardome",
    exact: true,
    component: Main,
  },
  emplant: {
    path: "/emplant",
    exact: true,
    component: Main,
  },
  bioarc: {
    path: "/bioarc",
    exact: true,
    component: Main,
  },
  dronescan: {
    path: "/dronescan",
    exact: true,
    component: Main,
  },
};

export const MenuGridRoutes = {
  "/preview": [
    {
      path: routes.floatTank.path,
      imgPath: "/images/floattank.jpg",
      name: "Float Tank",
    },
    {
      path: routes.floatTankScan.path,
      imgPath: "/images/scan3d.jpg",
      name: "3D Scan",
    },
    {
      path: routes.dome.path,
      imgPath: "/images/dome.jpg",
      name: "Geodesic Dome",
    },
    {
      path: routes.stardome.path,
      imgPath: "/images/dome.jpg",
      name: "Geodesic Dome",
    },
    {
      path: routes.emplant.path,
      imgPath: "/images/emplant.jpg",
      name: "Emplant",
    },
    // {
    //   path: routes.ebs.path,
    //   imgPath: "/images/ebs.jpg",
    //   name: "Ecobuild Systems",
    // },
    {
      path: routes.bioarc.path,
      imgPath: "/images/bioarc.jpg",
      name: "BioArc",
    },
    {
      path: routes.dronescan.path,
      imgPath: "/images/dronescan.jpg",
      name: "Drone Scan",
    },
    // {
    //   path: routes.viewer.path,
    //   imgPath: "/images/editor.jpg",
    //   name: "Editor",
    // },
  ],
};

// useStore.setState({ menuGridsItems: menuGridStore });
