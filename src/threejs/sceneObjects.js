export default class SceneObjects {
  //   constructor() {}

  static scenes = {};
  static transformControlsObj = null;

  static addScene(modelName, scene) {
    this.scenes[modelName] = scene;
  }

  // add a hash or counter for updates, pass that hash/ update counter to getScenes
  // with that create hook that can check if scene object has been updated
  static getScenes() {
    return this.scenes;
  }

  static removeScene(key) {
    delete this.scenes[key];
  }

  resetScenes() {
    this.scene = {};
  }

  static setTransformObj(obj) {
    this.transformControlsObj = obj;
  }

  static removeTransformObj() {
    this.transformControlsRef = null;
  }
}
